let express = require('express');
let proxy = require('http-proxy-middleware');
const cors = require('cors');
const fetch = require("node-fetch");
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
var imgur = require('imgur');


imgur.setClientId('6c863e3d0a6bf8f');




let app = express();

let request = require('request');

// app.use(function(req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//   next();
// });

let corsOptions = {
  origin: '*',
  optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
  Authorization: "Client-ID 6c863e3d0a6bf8f",
  "Access-Control-Allow-Origin" : "*"
}
app.use(cors(corsOptions));


app.get('/api/galleries', function(req,res) {
    let query = 'cats';
    let optionalParams = {sort: 'top', dateRange: 'week', page: 1, viral: 'false'}
    console.log(req.query);
    if(req.query.hasOwnProperty("keywords")){
        query = req.query["keywords"];
    }
    if(req.query.hasOwnProperty("options")){
        optionalParams = req.query["options"];
    }

  try {
      imgur.search(query, optionalParams)
          .then(function(json) {
              //console.log(json);
              res.send(json);
          })
          .catch(function (err) {
              console.error(err);
          });
  }catch(e){
    console.log(e);
  }
});

app.get('/api/gallery', function(req,res) {
    console.log(req.query);
    let id="";
    if(req.query.hasOwnProperty("id")){
        id = req.query["id"];
    }

    try {
        imgur.getAlbumInfo(id)
            .then(function(json) {
                console.log(json);
                res.send(json);
            })
            .catch(function (err) {
                console.error(err.message);
            });
    }catch(e){
        console.log(e);
    }
});


app.listen(7777);
console.log("Server started on port 7777");

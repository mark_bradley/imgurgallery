import {
  SET_GALLERIES,
  API_START,
  API_END,
  FETCH_GALLERIES,
  FETCH_GALLERY,
  SET_GALLERY
} from "../actions/types";

export default function(state = {}, action) {
  console.log("action type => ", action.type);
  switch (action.type) {
    case SET_GALLERIES:
      return { data: action.payload.data };
    case SET_GALLERY:
      return { gallery: action.payload.data };
    case API_START:
      if (action.payload === FETCH_GALLERIES || action.payload === FETCH_GALLERY) {
        return {
          ...state,
          isLoadingData: true
        };
      }
      break;
    case API_END:
      if (action.payload === FETCH_GALLERIES || action.payload === FETCH_GALLERY) {
        return {
          ...state,
          isLoadingData: false
        };
      }
      break;
    default:
      return state;
  }
}

import React, { Component } from "react";
import { connect } from "react-redux";
import styled from "styled-components";
import Navbar from "../components/Navbar";
import GalleryList from "../components/GalleryList";
import { fetchGalleries, fetchGallery } from "../actions";

const StyledApp = styled.div`
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;

  aside {
    min-width: 35vh;
    display: flex;
    justify-content: flex-end;
  }
  main {
   flex: 1 0 350px;
    ${"" /* not responsive */} padding: 0 1rem; 
  }
`;

class App extends Component {
  state = {
      data: []
  };

  //get initial images
  componentDidMount() {
    this.props.fetchGalleries();
  }

  componentWillReceiveProps(nextProps, nextContext){
    this.setState({data: nextProps.data});
  }

  search = (keywords, options) => {
      console.log("search",keywords);
      this.props.fetchGalleries({keywords: keywords, options: options});
  }

  fetchGallery = (id) => {
      this.props.fetchGallery(id);
  }

  goBack = () => {
      console.log("reset data");
      this.setState({data: this.props.data});
  }

  render() {
    console.log(this.props.data);

    return (
      <StyledApp>
        <Navbar search={this.search} back={this.goBack}/>
        <main>
          {this.props.isLoadingData ? (
            "Loading . . ."
          ) : (
            <GalleryList
              data={this.state.data ? this.state.data : []}
              fetch={this.fetchGallery}
            />
          )}
        </main>
      </StyledApp>
    );
  }
}

const mapStateToProps = ({ data = [], isLoadingData = false, gallery = [] }) => ({
  data,
  isLoadingData,
  gallery
});
export default connect(
  mapStateToProps,
  {
    fetchGalleries: fetchGalleries,
    fetchGallery
  }
)(App);

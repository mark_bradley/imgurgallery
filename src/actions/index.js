import { SET_GALLERIES, API, FETCH_GALLERIES, FETCH_GALLERY, SET_GALLERY } from "./types";

//Fetch galleries from our proxy server by issuing a Redux action
export function fetchGalleries(request) {
  console.log("searching", request);
  return apiAction({
    url: `http://localhost:7777/api/galleries`,
    onSuccess: setGalleries,
    onFailure: console.log('Error occured loading gallery list'),
    label: FETCH_GALLERIES,
    data: request
  });
}

//Once the data is returned set it
function setGalleries(data) {
    return {
        type: SET_GALLERIES,
        payload: data
    };
}

//This was not necessary as Gallery data is already included in the Galleries response
export function fetchGallery(id) {
    console.log("loading gallery", id);
    return apiAction({
        url: `http://localhost:7777/api/gallery`,
        onSuccess: setGallery,
        onFailure: console.log('Error occured loading gallery'),
        label: FETCH_GALLERY,
        data: {'id': id}
    });
}

//Set individual gallery after return from API
function setGallery(data) {
    return {
        type: SET_GALLERY,
        payload: data
    };
}

//Default action properties and returned properties
function apiAction({
  url = "",
  method = "GET",
  data = null,
  accessToken = null,
  onSuccess = () => {},
  onFailure = () => {},
  label = "",
  headersOverride = null
}) {
  return {
    type: API,
    payload: {
      url,
      method,
      data,
      accessToken,
      onSuccess,
      onFailure,
      label,
      headersOverride
    }
  };
}

export const FETCH_GALLERY = "FETCH_GALLERY";
export const SET_GALLERY = "SET_GALLERY";
export const FETCH_GALLERIES = "FETCH_GALLERIES";
export const SET_GALLERIES = "SET_GALLERIES";

export const API = "API";
export const API_START = "API_START";
export const API_END = "API_END";
export const ACCESS_DENIED = "ACCESS_DENIED";
export const API_ERROR = "API_ERROR";

import { createStore, applyMiddleware } from "redux";
import rootReducer from "../reducers";
import apiMiddleware from "../middleware/api";
import reduxLogger from "redux-logger"

const store = createStore(rootReducer, applyMiddleware(apiMiddleware, reduxLogger));
window.store = store;
export default store;

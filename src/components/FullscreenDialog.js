import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import ListItemText from '@material-ui/core/ListItemText';
import ListItem from '@material-ui/core/ListItem';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import Image from 'material-ui-image'

import ThumbDownIcon from '@material-ui/icons/ThumbDown';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import StarIcon from '@material-ui/icons/StarRate';
import ViewsIcon from '@material-ui/icons/Visibility';


const styles = {
    appBar: {
        position: 'relative',
    },
    flex: {
        display: 'flex',
        flex: 1,
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    imageIcons: {
        display: 'flex',
        flex: 1,
        alignItems: 'center'
    },
    flexColumn: {
        display: 'flex',
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center'
    },
    p: {
        backgroundColor: '#CCCCCC',
        padding: '10px 10px',
        fontWeight: 'bold'
    }
};

function Transition(props) {
    return <Slide direction="up" {...props} />;
}

class FullScreenDialog extends React.Component {
    state = {
        open: false,
        data: {}

    };

    componentWillReceiveProps(nextProps, nextContext){
        this.setState({
            open: nextProps.open,
            data: nextProps.data
        });
    }

    handleClickOpen = () => {
        this.setState({ open: true });
    };

    handleClose = () => {
        this.setState({ open: false });
    };

    render() {
        const { classes, data } = this.props;

        let sometext = "test";
        return (
            <div>
                <Dialog
                    fullScreen
                    open={this.state.open}
                    onClose={this.handleClose}
                    TransitionComponent={Transition}
                >
                    <AppBar className={classes.appBar}>
                        <Toolbar>
                            <IconButton color="inherit" onClick={this.handleClose} aria-label="Close">
                                <CloseIcon />
                            </IconButton>
                            <Typography variant="h6" color="inherit" className={classes.flexColumn}>
                                {data.title}
                            </Typography>
                            <Typography variant="h6" color="inherit" className={classes.flex}>
                                <span className={classes.imageIcons}>{(data.views) ? data.views : "0"} <ViewsIcon/></span>
                                <span className={classes.imageIcons}>{(data.ups ) ? data.ups : "0"} <ThumbUpIcon/></span>
                                <span className={classes.imageIcons}>{(data.downs) ? data.downs : "0" } <ThumbDownIcon/></span>
                                <span className={classes.imageIcons}>{(data.score) ? data.score : "0"} <StarIcon/></span>
                            </Typography>
                            {/*To do download button*/}
                            {/*<Button color="inherit" onClick={this.handleClose}>*/}
                                {/*Download*/}
                            {/*</Button>*/}
                        </Toolbar>
                    </AppBar>
                    {/*{ data.description ? <p className={classes.p}>{data.description}</p> : ""}*/}
                    <List>
                        <ListItem button>
                            <ListItemText primary="Title" secondary={(data.title) ? data.title : "Untitled"} />
                        </ListItem>
                        <Divider />
                        <ListItem button>
                            <ListItemText primary="Description" secondary={(data.description) ? data.description : "No description"} />
                        </ListItem>
                    </List>
                    <Image src={this.state.data.link} className={classes.image}/>
                </Dialog>
            </div>
        );
    }
}

FullScreenDialog.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(FullScreenDialog);

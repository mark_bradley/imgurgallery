import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Select from "@material-ui/core/Select/Select";
import Input from "@material-ui/core/Input/Input";
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from "@material-ui/core/MenuItem/MenuItem";
import Menu from "@material-ui/core/Menu/Menu";


const styles = theme => ({
    container: {
        display: 'flex',
        flexDirection: 'column'
    }
})

class FilterDialog extends React.Component {
    state = {
        open: false,
        sort: "top",
        window: "all",
        viral: false
    };

    componentWillReceiveProps(nextProps, nextContext){
        if(nextProps.open !== this.state.open) {
            this.setState({open: nextProps.open});
        }
    }

    handleClose = () => {
        this.setState({ open: false });
    };

    handleChange = (event) => {
        this.setState({ [event.target.name]: event.target.value });
    }

    handleFilters = event => {
        this.handleClose();
        this.props.setFilters({sort: this.state.sort, window: this.state.window, viral: this.state.viral});
    }

    render() {
        const {classes} = this.props;
        const filters = {sort: this.state.sort, window: this.state.window};
        const setFilters = this.props.setFilters;

        return (
            <div>
                <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                    aria-labelledby="form-dialog-title"
                >
                    <DialogTitle id="form-dialog-title">Filter Images</DialogTitle>
                    <DialogContent className={classes.container}>
                        <DialogContentText>
                            Configure your search criteria.
                        </DialogContentText>
                        <InputLabel shrink htmlFor="sort-options">
                            Sort
                        </InputLabel>
                        <Select
                            value={this.state.sort}
                            onChange={this.handleChange}
                            input={<Input name="sort" id="sort-options" />}
                        >
                            <MenuItem value={'time'}>Time</MenuItem>
                            <MenuItem value={'top'}>Top</MenuItem>
                            <MenuItem value={'viral'}>Viral</MenuItem>
                            <MenuItem value={'hot'}>Hot</MenuItem>
                        </Select>
                    </DialogContent>
                    <DialogContent className={classes.container}>
                        <InputLabel shrink htmlFor="window-options">
                            Time Window
                        </InputLabel>
                        <Select
                            value={this.state.window}
                            onChange={this.handleChange}
                            input={<Input name="window" id="window-options" />}
                        >
                            <MenuItem value={'all'}>All</MenuItem>
                            <MenuItem value={'week'}>Week</MenuItem>
                            <MenuItem value={'month'}>Month</MenuItem>
                            <MenuItem value={'year'}>Year</MenuItem>
                        </Select>
                    </DialogContent>
                    <DialogContent className={classes.container}>
                        <InputLabel shrink htmlFor="viral-options">
                            Show Viral
                        </InputLabel>
                        <Select
                            value={this.state.viral}
                            onChange={this.handleChange}
                            input={<Input name="viral" id="viral-options" />}
                        >
                            <MenuItem value={false}>False</MenuItem>
                            <MenuItem value={true}>True</MenuItem>
                        </Select>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={this.handleFilters} color="primary">
                            Set Filters
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

export default withStyles(styles)(FilterDialog)

import React from 'react';
import PropTypes from 'prop-types';
import FilterDialog from './FilterDialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import InputBase from '@material-ui/core/InputBase';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import {fade} from '@material-ui/core/styles/colorManipulator';
import {withStyles} from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import FilterIcon from '@material-ui/icons/Tune';
import GridOnIcon from '@material-ui/icons/GridOn';
import GridOffIcon from '@material-ui/icons/GridOff';
import BackIcon from '@material-ui/icons/ArrowBack';
import MoreIcon from '@material-ui/icons/MoreVert';
import Input from '@material-ui/core/Input';
import Select from '@material-ui/core/Select';

const styles = theme => ({
    root: {
        width: '100%',
    },
    grow: {
        flexGrow: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    title: {
        display: 'none',
        [theme.breakpoints.up('sm')]: {
            display: 'block',
        },
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: fade(theme.palette.common.white, 0.25),
        },
        marginRight: theme.spacing.unit * 2,
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing.unit * 3,
            width: 'auto',
        },
    },
    searchIcon: {
        width: theme.spacing.unit * 9,
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputRoot: {
        color: 'inherit',
        width: '100%',
    },
    inputInput: {
        paddingTop: theme.spacing.unit,
        paddingRight: theme.spacing.unit,
        paddingBottom: theme.spacing.unit,
        paddingLeft: theme.spacing.unit * 10,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
    },
    sectionDesktop: {
        display: 'none',
        [theme.breakpoints.up('md')]: {
            display: 'flex',
        },
    },
    sectionMobile: {
        display: 'flex',
        [theme.breakpoints.up('md')]: {
            display: 'none',
        },
    },
});

class PrimarySearchAppBar extends React.Component {
    state = {
        anchorEl: null,
        mobileMoreAnchorEl: null,
        grid: true,
        sort: 'top',
        window: 'all',
        viral: 'false',
        page: 1,
        filtersDialogOpen: false,
        search: ""
    };

    handleProfileMenuOpen = event => {
        this.setState({anchorEl: event.currentTarget});
    };

    handleMenuClose = () => {
        this.setState({anchorEl: null});
        this.handleMobileMenuClose();
    };

    handleMobileMenuOpen = event => {
        this.setState({mobileMoreAnchorEl: event.currentTarget});
    };

    handleMobileMenuClose = () => {
        this.setState({mobileMoreAnchorEl: null});
    };

    handleFilterOpen = event => {
        this.setState({filtersDialogOpen: true});
    };

    //To do: set validation and restrict character types
    handleInputChange = (event) => {
        this.setState({search: event.target.value})
    }

    handleSearchSubmit = (event) => {
        if (event.keyCode === 13) {
            event.preventDefault();
            event.stopPropagation();
            let options = {
                sort: this.state.sort,
                window: this.state.window,
                viral: this.state.viral,
                page: this.state.page
            };
            this.props.search(this.state.search, options);
            this.setState({filtersDialogOpen: false});
        }
    }

    //To do: show back button only when inside a gallery / album
    handleBack = event => {
        this.props.back();
    }

    //Receive the callback function from FilterDialog and set filters for searches
    setFilters = (state) => {
        console.log('state', state);
        this.setState({
            sort: state.sort,
            window: state.window,
            viral: state.viral
        })

        let options = {
            sort: this.state.sort,
            window: this.state.window,
            viral: this.state.viral,
            page: this.state.page
        };
        this.props.search(this.state.search, options);
    }

    //To Do: toggle layout
    toggleGrid = () => {
        console.log("toggle grid");
        // this.setState((state) => {
        //     // Important: read `state` instead of `this.state` when updating.
        //     return {grid: !state.grid}
        // });
    }

    render() {
        const {anchorEl, mobileMoreAnchorEl} = this.state;
        const {classes} = this.props;
        const isMenuOpen = Boolean(anchorEl);
        const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

        const renderMobileMenu = (
            <Menu
                anchorEl={mobileMoreAnchorEl}
                anchorOrigin={{vertical: 'top', horizontal: 'right'}}
                transformOrigin={{vertical: 'top', horizontal: 'right'}}
                open={isMobileMenuOpen}
                onClose={this.handleMobileMenuClose}
                onClick={this.handleMenuClick}
            >
                <MenuItem>
                    <IconButton color="inherit"
                                onClick={this.handleBack}>
                        <BackIcon/> Back
                    </IconButton>
                </MenuItem>
                {/*<MenuItem>*/}
                    {/*<IconButton color="inherit">*/}
                        {/*<GridOnIcon/>*/}
                    {/*</IconButton>*/}
                {/*</MenuItem>*/}
                <MenuItem>
                    <IconButton
                        aria-haspopup="true"
                        onClick={this.handleFilterOpen}
                        color="inherit"
                    >
                        <FilterIcon/>
                    </IconButton>
                </MenuItem>
            </Menu>
        );

        return (
            <div className={classes.root}>
                <FilterDialog setFilters={this.setFilters} open={this.state.filtersDialogOpen}/>
                <AppBar position="static">
                    <Toolbar>
                        <IconButton className={classes.menuButton} color="inherit" aria-label="Open drawer">
                            <MenuIcon/>
                        </IconButton>
                        <Typography className={classes.title} variant="h6" color="inherit" noWrap>
                            Imgur Gallery Explorer
                        </Typography>
                        <div className={classes.search}>
                            <div className={classes.searchIcon}>
                                <SearchIcon/>
                            </div>
                            <InputBase
                                placeholder="Search…"
                                onChange={this.handleInputChange}
                                onKeyDown={this.handleSearchSubmit}
                                classes={{
                                    root: classes.inputRoot,
                                    input: classes.inputInput,
                                }}
                            />
                        </div>
                        <div className={classes.grow}/>
                        <div className={classes.sectionDesktop}>
                            <IconButton color="inherit"
                                        onClick={this.handleBack}>
                                <BackIcon/> Back
                            </IconButton>
                            {/*<IconButton color="inherit">*/}
                                {/*<GridOnIcon/>*/}
                            {/*</IconButton>*/}
                            <IconButton
                                aria-haspopup="true"
                                onClick={this.handleFilterOpen}
                                color="inherit"
                            >
                                <FilterIcon/>
                            </IconButton>
                        </div>
                        <div className={classes.sectionMobile}>
                            <IconButton aria-haspopup="true" onClick={this.handleMobileMenuOpen} color="inherit">
                                <MoreIcon/>
                            </IconButton>
                        </div>
                    </Toolbar>
                </AppBar>
                {renderMobileMenu}
            </div>
        );
    }
}

PrimarySearchAppBar.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(PrimarySearchAppBar);

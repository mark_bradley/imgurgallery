import React from "react";
import Image from 'material-ui-image'
import FullScreenDialog from "../components/FullscreenDialog";
import styled from "styled-components";


const StyledGallery = styled.div`
  h1 {
    font-weight: 700;
    font-size: 36px;
    color: #000000;
    margin: 0;
  }
  h4 {
    font-weight: 700;
    font-size: 20px;
    color: #000000;
    margin: 0.5rem 0 4rem 0;
  }
  p {
    font-weight: 300;
    font-size: 18px;
    color: #4a4a4a;
  }
`;

const StyledList  = styled.div`{
    display: flex;
    flex-wrap: wrap;
    width: 100vw;
    justify-content: space-around;
`

const ImageContainer = styled.div`{
    display: 'flex';
    flex-direction: 'column';
    width: 30%;
    height 30%;
    margin: 10px 10px;
    
    cursor: pointer;
    
    @media (max-width: 768px) {
        width: 100%;
        height: 100%;
    }
    
    p {
        font-weight: 300;
        font-size: 18px;
        color: #4a4a4a;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
    }
}`

class GalleryList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            title: this.props.title,
            data: this.props.data,
            displayed: [],
            open: false,
            currentImage: {}
        };
    }

    componentDidMount() {

    }

    componentWillReceiveProps(nextProps){
        console.log("receive props", nextProps);
        this.setState({displayed: nextProps.data});
    }

    handleGalleryImageSelection = (event) => {
        console.log("click on gallery", event.currentTarget.getAttribute('data-id'));
        //image data already included in gallery data
        //this.props.fetch(event.currentTarget.getAttribute('data-id'));
        const galleryId = event.currentTarget.getAttribute('data-id');
        let filteredGalleries = this.state.data.filter((gallery) => {
            return gallery.id === galleryId;
        })[0];

        let imageFound = this.state.displayed.find(image  =>
            galleryId === image.id
        );

        console.log('image found?',imageFound);
        console.log(filteredGalleries);


        //console.log(filteredGalleries, filteredGalleries[0].hasOwnProperty("images"));
        if(filteredGalleries && !filteredGalleries.hasOwnProperty('images')){
            console.log("condition 1");
            this.setState({
                currentImage: filteredGalleries,
                open: true
            });

            return;
        }

        if(filteredGalleries && filteredGalleries["images"].length > 1) {
            console.log("condition 2");
            console.log(filteredGalleries[0])
            this.setState({
                displayed: filteredGalleries.images,
                open: false
            });

            return;
        }else if(filteredGalleries && filteredGalleries.images.length === 1){
            console.log("condition 3");
            this.setState({
                currentImage: filteredGalleries.images[0],
                open: true,
            });

            return;
        }

        if(imageFound ){
            console.log("condition 4");
            this.setState({
                currentImage: imageFound,
                open: true
            });

            return;
        }

    }



    render() {
        const {data = [], gallery = {"images":[]} } = this.props;

        return (
            <StyledGallery>
                <FullScreenDialog open={this.state.open} data={this.state.currentImage}/>
                <StyledList>
                    {this.state.displayed.map(gallery => (
                        <React.Fragment>
                            <ImageContainer key={gallery.id} data-id={gallery.id} onClick={this.handleGalleryImageSelection}>
                                <p>{(gallery.title) ? gallery.title : "Untitled"}</p>
                                {/*<caption>{gallery.title}</caption>*/}
                                { (gallery.images) ?
                                    <Image src={gallery.images[0].link}></Image> :
                                    <Image src={gallery.link}></Image>
                                }
                            </ImageContainer>
                        </React.Fragment>
                    ))}
                    {/*{(gallery.images) ? gallery.images.map((image)=>(*/}
                        {/*<div key={image.id}>*/}
                            {/*<Image src={image.link}></Image>*/}
                            {/*/!*<caption>{image.title}</caption>*!/*/}
                        {/*</div>*/}
                    {/*)) : <div></div>}*/}
                </StyledList>
            </StyledGallery>
        );
    }
};

export default GalleryList;

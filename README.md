## Description

This app uses React Redux UI-Material Express and Node-imgur library to create
an Imgur gallery browser. Type a keyword into the search field and press enter to search.
Click an image to load the album or image details.

## Installation

```bash
git clone <this repo url>
cd <repo>
npm install
```

## Get started

```bash
In one terminal / command prompt run "npm run server"
In a second terminal / command prompt run "npm start"
```

## Notes:

* There likely is bugs or improvements to be made. With more time = better quality
* Some templating is taken from Material UI examples and modified
* Redux used mainly for API calls and callbacks for components + children

## To Do

* refactor some logic, such as using Autodux or Redux-actions
* clean up styling to be consistent on which style library to use
* Show back button only when inside album
* Add download button functionality

## Task description

Implement a simple web app that allows one to browse the Imgur gallery using https://api.imgur.com/:

* show gallery images in a grid of thumbnails;
* show image description in the thumbnail, top or bottom;
* allow selecting the gallery section: hot, top, user;
* allow including / excluding viral images from the result set;
* allow specifying window and sort parameters;
* when clicking an image in the gallery - show its details: big image, title, description, upvotes, downvotes and score.

Bonus points:

* instead of calling Imgur API directly, proxy the API calls through your server (ex. Express);

App requirements:

* use ReactJS;
* and preferably Redux.
